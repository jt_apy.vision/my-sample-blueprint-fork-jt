"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlueprintRegistry = exports.BlueprintKeys = void 0;
const requestContext_1 = require("../common/requestContext");
const aaveV2AvalancheBlueprint_1 = require("../extensions/aave_v2/aave_v2_avalanche/aaveV2AvalancheBlueprint");
const aaveV2EthBlueprint_1 = require("../extensions/aave_v2/aave_v2_eth/aaveV2EthBlueprint");
const aaveV2PolygonBlueprint_1 = require("../extensions/aave_v2/aave_v2_polygon/aaveV2PolygonBlueprint");
const acrossBlueprint_1 = require("../extensions/across_eth/acrossBlueprint");
const compoundV3EthBlueprint_1 = require("../extensions/compound_v3_eth/compoundV3EthBlueprint");
const ethBlueprint_1 = require("../extensions/ethereum_staking/ethBlueprint");
const gammaFinancePolygonBlueprint_1 = require("../extensions/gamma_finance_polygon/gammaFinancePolygonBlueprint");
const lidoEthBlueprint_1 = require("../extensions/lido_eth/lidoEthBlueprint");
const maticStakingV1EthBlueprint_1 = require("../extensions/matic_staking_v1_eth/maticStakingV1EthBlueprint");
const orcaWhirpoolsSolanaBlueprint_1 = require("../extensions/orca_whirpools_solana/orcaWhirpoolsSolanaBlueprint");
const pancakeswapV2BscBlueprint_1 = require("../extensions/pancakeswap_v2_bsc/pancakeswapV2BscBlueprint");
const simpleBlueprint_1 = require("../extensions/simple_blueprint_template/simpleBlueprint");
const simpleBlueprintWithShares_1 = require("../extensions/simple_blueprint_template/simpleBlueprintWithShares");
const sushiswapEthBlueprint_1 = require("../extensions/sushiswap_eth/sushiswapEthBlueprint");
const sushiswapFarmingV1EthBlueprint_1 = require("../extensions/sushiswap_farming_v1_eth/sushiswapFarmingV1EthBlueprint");
const trisolarisAuroraBlueprint_1 = require("../extensions/trisolaris_aurora/trisolarisAuroraBlueprint");
const uniswapV3BscBlueprint_1 = require("../extensions/uniswap_v3_bsc/uniswapV3BscBlueprint");
const uniswapv2EthBlueprint_1 = require("../extensions/uniswapv2_eth/uniswapv2EthBlueprint");
const verseBlueprint_1 = require("../extensions/verse/verseBlueprint");
const NetworkConfigurations_1 = require("web3-wrapper-library/lib/NetworkConfigurations");
var BlueprintKeys;
(function (BlueprintKeys) {
    BlueprintKeys["SIMPLE_BLUEPRINT_TEMPLATE"] = "simple_blueprint_eth";
    BlueprintKeys["SIMPLE_BLUEPRINT_WITH_SHARES_TEMPLATE"] = "simple_blueprint_with_shares_eth";
    BlueprintKeys["UNISWAP_V2_ETH"] = "uniswapv2_eth";
    BlueprintKeys["MATIC_STAKING_V1_ETH"] = "matic_staking_v1_eth";
    BlueprintKeys["SUSHISWAP_ETH"] = "sushiswap_eth";
    BlueprintKeys["LIDO_ETH"] = "lido_eth";
    BlueprintKeys["SUSHISWAP_FARMING_V1_ETH"] = "sushiswap_farming_v1_eth";
    BlueprintKeys["ETHEREUM_STAKING"] = "ethereum_staking_eth";
    BlueprintKeys["COMPOUND_V3_ETH"] = "compound_v3_eth";
    BlueprintKeys["PANCAKESWAP_V2_BSC"] = "pancakeswap_v2_bsc";
    BlueprintKeys["ORCA_WHIRPOOLS_SOLANA"] = "orca_whirpools_solana";
    BlueprintKeys["GAMMA_FINANCE_POLYGON"] = "gamma_finance_polygon";
    BlueprintKeys["VERSE_FINANCE_ETH"] = "verse_eth";
    BlueprintKeys["AAVE_V2_ETH"] = "aave_v2_eth";
    BlueprintKeys["UNISWAP_V3_BSC"] = "uniswap_v3_bsc";
    BlueprintKeys["AAVE_V2_POLYGON"] = "aave_v2_polygon";
    BlueprintKeys["AAVE_V2_AVALANCHE"] = "aave_v2_avalanche";
    BlueprintKeys["TRISOLARIS_AURORA"] = "trisolaris_aurora";
    BlueprintKeys["ACROSS_ETH"] = "across_eth";
})(BlueprintKeys = exports.BlueprintKeys || (exports.BlueprintKeys = {}));
class BlueprintRegistry {
    constructor(cache, configService) {
        const ethereumContext = new requestContext_1.RequestContext(NetworkConfigurations_1.CHAINID.ETHEREUM, cache, configService);
        const bscContext = new requestContext_1.RequestContext(NetworkConfigurations_1.CHAINID.BSC, cache, configService);
        const solanaContext = new requestContext_1.RequestContext(NetworkConfigurations_1.CHAINID.SOLANA, cache, configService);
        const polygonContext = new requestContext_1.RequestContext(NetworkConfigurations_1.CHAINID.MATIC, cache, configService);
        const avalancheContext = new requestContext_1.RequestContext(NetworkConfigurations_1.CHAINID.AVAX, cache, configService);
        const auroraContext = new requestContext_1.RequestContext(NetworkConfigurations_1.CHAINID.AURORA, cache, configService);
        this.blueprints = new Map();
        // simple blueprint template
        this.blueprints.set(BlueprintKeys.SIMPLE_BLUEPRINT_TEMPLATE, new simpleBlueprint_1.SimpleBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.SIMPLE_BLUEPRINT_TEMPLATE)));
        this.blueprints.set(BlueprintKeys.SIMPLE_BLUEPRINT_WITH_SHARES_TEMPLATE, new simpleBlueprintWithShares_1.SimpleBlueprintWithShares(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.SIMPLE_BLUEPRINT_WITH_SHARES_TEMPLATE)));
        // ethereum
        this.blueprints.set(BlueprintKeys.ACROSS_ETH, new acrossBlueprint_1.AcrossBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.ACROSS_ETH)));
        this.blueprints.set(BlueprintKeys.UNISWAP_V2_ETH, new uniswapv2EthBlueprint_1.Uniswapv2EthBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.UNISWAP_V2_ETH)));
        this.blueprints.set(BlueprintKeys.MATIC_STAKING_V1_ETH, new maticStakingV1EthBlueprint_1.MaticStakingV1EthBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.MATIC_STAKING_V1_ETH)));
        this.blueprints.set(BlueprintKeys.SUSHISWAP_ETH, new sushiswapEthBlueprint_1.SushiswapEthBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.SUSHISWAP_ETH)));
        this.blueprints.set(BlueprintKeys.LIDO_ETH, new lidoEthBlueprint_1.LidoEthBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.LIDO_ETH)));
        this.blueprints.set(BlueprintKeys.SUSHISWAP_FARMING_V1_ETH, new sushiswapFarmingV1EthBlueprint_1.SushiswapFarmingV1EthBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.SUSHISWAP_FARMING_V1_ETH)));
        this.blueprints.set(BlueprintKeys.ETHEREUM_STAKING, new ethBlueprint_1.EthereumBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.ETHEREUM_STAKING)));
        this.blueprints.set(BlueprintKeys.COMPOUND_V3_ETH, new compoundV3EthBlueprint_1.CompoundV3EthBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.COMPOUND_V3_ETH)));
        this.blueprints.set(BlueprintKeys.VERSE_FINANCE_ETH, new verseBlueprint_1.VerseBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.VERSE_FINANCE_ETH)));
        this.blueprints.set(BlueprintKeys.AAVE_V2_ETH, new aaveV2EthBlueprint_1.AaveV2EthBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(ethereumContext, BlueprintKeys.AAVE_V2_ETH)));
        // bsc
        this.blueprints.set(BlueprintKeys.PANCAKESWAP_V2_BSC, new pancakeswapV2BscBlueprint_1.PancakeswapV2BscBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(bscContext, BlueprintKeys.PANCAKESWAP_V2_BSC)));
        this.blueprints.set(BlueprintKeys.UNISWAP_V3_BSC, new uniswapV3BscBlueprint_1.UniswapV3BscBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(bscContext, BlueprintKeys.UNISWAP_V3_BSC)));
        // solana
        this.blueprints.set(BlueprintKeys.ORCA_WHIRPOOLS_SOLANA, new orcaWhirpoolsSolanaBlueprint_1.OrcaWhirpoolsSolanaBlueprint(solanaContext));
        // polygon
        this.blueprints.set(BlueprintKeys.GAMMA_FINANCE_POLYGON, new gammaFinancePolygonBlueprint_1.GammaFinancePolygonBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(polygonContext, BlueprintKeys.GAMMA_FINANCE_POLYGON)));
        this.blueprints.set(BlueprintKeys.AAVE_V2_POLYGON, new aaveV2PolygonBlueprint_1.AaveV2PolygonBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(polygonContext, BlueprintKeys.AAVE_V2_POLYGON)));
        // avalanche
        this.blueprints.set(BlueprintKeys.AAVE_V2_AVALANCHE, new aaveV2AvalancheBlueprint_1.AaveV2AvalancheBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(avalancheContext, BlueprintKeys.AAVE_V2_AVALANCHE)));
        // aurora
        this.blueprints.set(BlueprintKeys.TRISOLARIS_AURORA, new trisolarisAuroraBlueprint_1.TrisolarisAuroraBlueprint(requestContext_1.RequestContext.buildWithBlueprintId(auroraContext, BlueprintKeys.TRISOLARIS_AURORA)));
    }
    getBlueprint(blueprintKey) {
        return this.blueprints.get(blueprintKey);
    }
    getChildrenBlueprintFromParent(parentBlueprintId) {
        return [...this.blueprints.values()].filter((item) => {
            return item.getParentBlueprintId() === parentBlueprintId;
        });
    }
    blueprintHasChildren(blueprintId) {
        const result = this.getChildrenBlueprintFromParent(blueprintId);
        if (result === undefined || result.length === 0) {
            return false;
        }
        return true;
    }
}
exports.BlueprintRegistry = BlueprintRegistry;
