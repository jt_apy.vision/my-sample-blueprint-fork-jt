"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPositionDataResultDto = void 0;
class UserPositionDataResultDto {
    constructor(userAddress, blueprintKey, contractAddress, contractName, underlyingTokens, receiptTokens, positionSnapshotCount, positionAgeSeconds, totalFeeUsd, positionSnapshots, dayData) {
        this.userAddress = userAddress;
        this.blueprintKey = blueprintKey;
        this.contractAddress = contractAddress;
        this.contractName = contractName;
        this.underlyingTokens = underlyingTokens;
        this.receiptTokens = receiptTokens;
        this.positionSnapshotCount = positionSnapshotCount;
        this.positionAgeSeconds = positionAgeSeconds;
        this.totalFeeUsd = totalFeeUsd;
        this.positionSnapshots = positionSnapshots;
        this.dayData = dayData;
    }
}
exports.UserPositionDataResultDto = UserPositionDataResultDto;
