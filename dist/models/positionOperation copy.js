"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PositionOperation = void 0;
const bignumber_js_1 = __importDefault(require("bignumber.js"));
class PositionOperation {
    constructor(
    // deposit | withdrawal | income | transfer_in | transfer_out | null_op
    operation, 
    // unique identifier
    positionIdentifier, 
    // tokens sent from user to protocol
    inputTokens, 
    // tokens sent from protocol to user
    outputTokens, 
    // gas token amount spent for transaction
    gasTokenAmount = (0, bignumber_js_1.default)(0), positionShareDetails) {
        this.operation = operation;
        this.positionIdentifier = positionIdentifier;
        this.inputTokens = inputTokens;
        this.outputTokens = outputTokens;
        this.gasTokenAmount = gasTokenAmount;
        this.positionShareDetails = positionShareDetails;
    }
}
exports.PositionOperation = PositionOperation;
