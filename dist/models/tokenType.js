"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenType = void 0;
var TokenType;
(function (TokenType) {
    TokenType[TokenType["RECEIPT"] = 0] = "RECEIPT";
    TokenType[TokenType["UNDERLYING"] = 1] = "UNDERLYING";
    TokenType[TokenType["INCOME"] = 2] = "INCOME";
})(TokenType = exports.TokenType || (exports.TokenType = {}));
