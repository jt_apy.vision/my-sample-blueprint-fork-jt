"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenBalanceDto = void 0;
class TokenBalanceDto {
    constructor(balance, cumulativeValueUsd) {
        this.balance = balance;
        this.cumulativeValueUsd = cumulativeValueUsd;
    }
}
exports.TokenBalanceDto = TokenBalanceDto;
